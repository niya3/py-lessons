# Основы программирования на примере языка Python3, часть 1.

* Проблема курицы и яйца - нужно постоянно забегать вперед
* Цель занятия:
  * научиться основным используемым в следующих занятия инструментам и понятиям
  * освоить (минимально) достаточное для самостоятельной работы подмножество языка

## Характерные черты Python.
### What is Python
Python - это интерпретируемый высокоуровневый язык программирования общего назначения. Типизация строгая динамическая утиная типизацией.
* язык программирования(vs естественный язык)
* высокоуровневый(vs машинные коды)
* общего назначения(vs DSL: Matlab, HTML, SQL...)
* интерпретируемый(vs компилируемый)
* строгая типизация(vs слабая)
* динамическая типизация(vs статическая)
* утиная типизация

### Чем хорош объективно
* Кроссплатформенность
* Батарейки в комплекте
* Множество модулей
* Динамическая типизация

### Чем плох?
* Медленное исполнение(компенсируется Cython)
* Нет параллельности из коробки(сложнее, чем в go)
* Динамическая типизация(ошибки в рантайме)

### Человеский фактор
* Широко распространён
* Просто начать изучать и использовать
* Просто разрабатывать(большинство Design Patterns [уже в языке](https://www.youtube.com/watch?v=Er5K_nR5lDQ))
* Легко читать
* Много вакансий

### Применение Python
Прикладное ПО, игры, веб-программирование, системной администрирование, научные расчеты, встраиваемые системы, 
https://www.mvoronin.pro/ru/blog/post-75
### Python2 vs Python3
Python2.7 EOL 2020, не содержит нужных фич `->` говорим только о Python3
### Реализации
PyPy, IronPython, QPython, Stackless, Jython, **CPython**

## Запускаем
REPL
### Интерпретатор
python, ipython, jupyter, онлайн
### скрипты
### замораживаем


## Основы программирования
Парадигма: императивная, структурная
### Алгоритм и программа
Задача, блок-схема, решение

### Синтаксис
* отступы
* комментарии

### Переменные
* Логический тип
* Присваивание, точнее связывание

#### встроенные типы.
* Логический тип
* Числа - целые, не-целые
* Контейнеры
  * списки - изменяемая длина, любые типы(в отличии от аррау), изменяемые элементы, обращение по индексу
  * кортежи
  * словари
* (не)изменяемые типы

### Условные переходы и циклы.
* Основа структурного программирования, теорема Бёма-Якопини (алгоритм `->` последовательность, ветвление, цикл). Ура, вы программисты.
* Логические операции, операторы сравнения, интерпретация как логический тип
* ветвления: if, elif, else
* циклы и итерации
* Некоторые встроенные функции

### Ввод и вывод.

stdout, stdin, stderr

## Строки
* Строка - неизменяемый список односимвольных строк
* Unicode
* Некоторые методы строк
* Форматирование строк


## Продолжаем учиться
### Ошибки и обработка
Python лишен ошибок времени компиляции и связывания, ошибки времени исполнения

* Синтксические ошибки
  * типичная ошибка
* Исключения
  * несоклько типичных исключений(выход за пределы, вызов неизвестного метода, попытка изменить неизменимый тип)

### Отладка
* print(stderr)
* pdb
* ipdb
* IDE

## Ещё глубже
## Поиск справочной информации.
* help
* python.org
* локальное зеркало

### Учебники и курсы
* https://pep8.ru/doc/tutorial-3.1/
* https://compscicenter.ru/courses/python/2018-spring/classes/

### Задачки
* Проект Эйлера
* http://www.checkio.org/
